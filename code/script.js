/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getDetails(animals)
{
    switch (animals)
    {
        case "cats":
            document.getElementById("modal-header").innerHTML = 'Cat Facts';
            mydata = readJSON('api/facts/cat.json');
            break;
        case "dog":
            document.getElementById("modal-header").innerHTML = 'Dog Facts';
            mydata = readJSON('api/facts/dog.json');
            break;
        case "horses":
            document.getElementById("modal-header").innerHTML = 'Horse Facts';
            mydata = readJSON('api/facts/horse.json');
            break;
        case "snails":
            document.getElementById("modal-header").innerHTML = 'Snail Facts';
            mydata = readJSON('api/facts/snail.json');
            break;
    }

    var modal = document.getElementById("modal");
    modal.style.display = "block";
}
function readJSON(path) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var obj = this.responseText;
            var obj1 = JSON.parse(obj);

            for (var key in obj1) {
                // skip loop if the property is from prototype
                if (!obj1.hasOwnProperty(key))
                    continue;

                var obj = obj1[key];
                var html = "";
                for (var prop in obj) {
                    // skip loop if the property is from prototype
                    if (!obj.hasOwnProperty(prop))
                        continue;

                    var fname = "";
                    var lname = "";
                    if (typeof obj[prop].user !== 'undefined') {
                        var fname = obj[prop].user.name.first;
                        var lname = obj[prop].user.name.last;
                    }

                    html += "<section><span class='fact-text'>" + obj[prop].text + "</span><span class='fact-author'>"
                            + fname + " " + lname + "</span></section>";
                    document.getElementById("content").innerHTML = html;
                }
            }
        }
    };
    xhttp.open("GET", path, true);
    xhttp.send();
}

function closeModal()
{
    var modal = document.getElementById("modal");
    modal.style.display = "none";
}